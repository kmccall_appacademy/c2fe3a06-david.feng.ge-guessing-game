# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.


def shuffle_file(input_name)
  lines = File.readlines(input_name)
  lines.shuffle!
  File.open("#{input_name}-shuffled.txt", "w") do |f|
    f.puts lines

  end

end



def guessing_game
  target = rand(1..100)
  guess = 0
  guesses = []
  until guess == target
    puts "guess a number"
    guess = gets.chomp.to_i
    guesses << guess
    p guesses
    if guess > target
      puts "too high"
    else
      puts "too low"
    end
  end
  puts "The number is #{target}, you guessed #{guesses.length} times."
end

if __FILE__ == $PROGRAM_NAME
  shuffle_file("test.txt")
end
